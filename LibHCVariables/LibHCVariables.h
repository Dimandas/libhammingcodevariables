#pragma once

#include <vector>

namespace LibHCVariables
{
	template<typename T>
	class HC
	{
	public:
		HC(T data);
		~HC();

		T Data();


		HC operator + (HC right);
		HC operator - (HC right);
		HC operator * (HC right);
		HC operator / (HC right);

		HC operator = (HC right);

		bool operator < (HC right);
		bool operator <= (HC right);
		bool operator > (HC right);
		bool operator >= (HC right);
		bool operator == (HC right);
		bool operator != (HC right);


		HC operator + (T right);
		HC operator - (T right);
		HC operator * (T right);
		HC operator / (T right);

		HC operator = (T right);

		bool operator < (T right);
		bool operator <= (T right);
		bool operator > (T right);
		bool operator >= (T right);
		bool operator == (T right);
		bool operator != (T right);

	protected:
		T value;

		std::vector<bool> code;

		void Generate();
		/*void Check(); // TODO enum add
		void Fix(); // TODO enum add */
		void CheckAndFix(); // TODO enum add
	};
}