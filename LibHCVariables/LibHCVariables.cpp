﻿#include "LibHCVariables.h"

#include <cmath>
#include <stdexcept>
#include <string>

using namespace LibHCVariables;

template<typename T>
HC<T>::HC(T data)
{
	value = data;

	Generate();
}

template<typename T>
HC<T>::~HC()
{

}

template<typename T>
T HC<T>::Data()
{
	CheckAndFix();

	return value;
}

template<typename T>
void HC<T>::Generate()
{
	size_t countBitMessage = sizeof(value) * 8;

	size_t countKeyBitCode;
	for (size_t i = 0; std::pow(2, i) <= countBitMessage; i++)
	{
		countKeyBitCode = i + 1;
	}

	code.reserve(countBitMessage + countKeyBitCode);

	{
		size_t countSetBitMessage = 0;
		size_t countSetKeyBitCode = 0;

		for (size_t i = 0; i < countBitMessage + countKeyBitCode; i++)
		{
			if (std::pow(2, countSetKeyBitCode) == i + 1 && (countSetKeyBitCode < countKeyBitCode))
			{
				code.push_back(false);

				countSetKeyBitCode = countSetKeyBitCode + 1;
			}
			else
			{
				bool bit = value & (1 << (countBitMessage - countSetBitMessage - 1));
				code.push_back(bit);

				countSetBitMessage = countSetBitMessage + 1;
			}
		}
	}

	for (size_t i = 0; i < countKeyBitCode; i++)
	{
		size_t countInterval = (size_t)std::pow(2, i);
		size_t countReadBit = 0;
		size_t countMissBit = 0;
		size_t sumBit = 0;

		for (size_t m = (size_t)std::pow(2, i) - 1; m < countBitMessage + countKeyBitCode; m++)
		{
			if (countMissBit == 0 && countReadBit != countInterval)
			{
				sumBit = sumBit + code[m];

				countReadBit = countReadBit + 1;
			}
			else
			{
				countMissBit = countMissBit + 1;
			}

			if (countMissBit == countInterval)
			{
				countMissBit = 0;
				countReadBit = 0;
			}

		}

		code[countInterval - 1] = sumBit % 2;
	}
}

/*
template<typename T>
void HC<T>::Check()
{

}

template<typename T>
void HC<T>::Fix()
{

}
*/

template<typename T>
void HC<T>::CheckAndFix()
{
	std::vector<bool> newCode;
	size_t countBitMessage = sizeof(value) * 8;

	size_t countKeyBitCode;
	for (size_t i = 0; std::pow(2, i) <= countBitMessage; i++)
	{
		countKeyBitCode = i + 1;
	}

	newCode.reserve(countBitMessage + countKeyBitCode);

	{
		size_t countSetBitMessage = 0;
		size_t countSetKeyBitCode = 0;

		for (size_t i = 0; i < countBitMessage + countKeyBitCode; i++)
		{
			if (std::pow(2, countSetKeyBitCode) == i + 1 && (countSetKeyBitCode < countKeyBitCode))
			{
				newCode.push_back(false);

				countSetKeyBitCode = countSetKeyBitCode + 1;
			}
			else
			{
				bool bit = value & (1 << (countBitMessage - countSetBitMessage - 1));
				newCode.push_back(bit);

				countSetBitMessage = countSetBitMessage + 1;
			}
		}
	}

	for (size_t i = 0; i < countKeyBitCode; i++)
	{
		size_t countInterval = (size_t)std::pow(2, i);
		size_t countReadBit = 0;
		size_t countMissBit = 0;
		size_t sumBit = 0;

		for (size_t m = (size_t)std::pow(2, i) - 1; m < countBitMessage + countKeyBitCode; m++)
		{
			if (countMissBit == 0 && countReadBit != countInterval)
			{
				sumBit = sumBit + newCode[m];

				countReadBit = countReadBit + 1;
			}
			else
			{
				countMissBit = countMissBit + 1;
			}

			if (countMissBit == countInterval)
			{
				countMissBit = 0;
				countReadBit = 0;
			}

		}

		newCode[countInterval - 1] = sumBit % 2;
	}

	bool errorFind = false;
	size_t numberErrorBit = 0;
	for (size_t i = 0; i < countKeyBitCode; i++)
	{
		size_t m = (size_t)std::pow(2, i) - 1;

		if (newCode[m] != code[m])
		{
			errorFind = true;
			numberErrorBit = numberErrorBit + m;
		}
	}

	if (errorFind == true)
	{
		value |= (1 << (countBitMessage - numberErrorBit - 1));

		Generate();

		throw std::runtime_error("Detect Error Variable. Attempt Fix. Error bit: " + std::to_string(numberErrorBit));
	}

}

template<typename T>
HC<T> HC<T>::operator + (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	HC<T> out(value + right.value);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator - (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	HC<T> out(value - right.value);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator * (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	HC<T> out(value * right.value);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator / (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	HC<T> out(value / right.value);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator = (HC right)
{
	right.CheckAndFix();

	value = right.value;

	Generate();

	return *this;
}

template<typename T>
bool HC<T>::operator < (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value < right.value);
}

template<typename T>
bool HC<T>::operator <= (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value <= right.value);
}

template<typename T>
bool HC<T>::operator > (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value > right.value);
}

template<typename T>
bool HC<T>::operator >= (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value >= right.value);
}

template<typename T>
bool HC<T>::operator == (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value == right.value);
}

template<typename T>
bool HC<T>::operator != (HC right)
{
	CheckAndFix();
	right.CheckAndFix();

	return (value != right.value);
}

template<typename T>
HC<T> HC<T>::operator + (T right)
{
	CheckAndFix();

	HC<T> out(value + right);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator - (T right)
{
	CheckAndFix();

	HC<T> out(value - right);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator * (T right)
{
	CheckAndFix();

	HC<T> out(value * right);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator / (T right)
{
	CheckAndFix();

	HC<T> out(value / right);
	out.Generate();

	return out;
}

template<typename T>
HC<T> HC<T>::operator = (T right)
{
	value = right;

	Generate();

	return *this;
}

template<typename T>
bool HC<T>::operator < (T right)
{
	CheckAndFix();

	return (value < right);
}

template<typename T>
bool HC<T>::operator <= (T right)
{
	CheckAndFix();

	return (value <= right);
}

template<typename T>
bool HC<T>::operator > (T right)
{
	CheckAndFix();

	return (value > right);
}

template<typename T>
bool HC<T>::operator >= (T right)
{
	CheckAndFix();

	return (value >= right);
}

template<typename T>
bool HC<T>::operator == (T right)
{
	CheckAndFix();

	return (value == right);
}

template<typename T>
bool HC<T>::operator != (T right)
{
	CheckAndFix();

	return (value != right);
}